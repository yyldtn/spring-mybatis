package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pojo.TbUser;
import com.service.TbUserService;
import com.yyl.common.collection.ListData;

@Controller
public class UserController {

	@Autowired
	private TbUserService tbUserService;
	
	@RequestMapping(value="/user/{userId}", method = RequestMethod.GET)
	@ResponseBody
	TbUser getUserById(@PathVariable Long userId) {
		
		return tbUserService.getUserById(userId);
	}
	
	@RequestMapping(value="/user", method = RequestMethod.GET)
	@ResponseBody
	ListData<TbUser> listUsers(int page, int rows) {
		
		return tbUserService.listUsers(page, rows);
	}
}
