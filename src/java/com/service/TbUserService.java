package com.service;

import com.pojo.TbUser;
import com.yyl.common.collection.ListData;

public interface TbUserService {

	TbUser getUserById(Long id);
	
	ListData<TbUser> listUsers(int page, int rows);
	
	TbUser deleteUserById(Long id);
	
	TbUser updateUserById(Long id);
	
	
}
