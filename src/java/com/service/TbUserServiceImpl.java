package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pojo.TbUser;
import com.pojo.TbUserExample;
import com.pojo.TbUserExample.Criteria;
import com.yyl.common.collection.ListData;
import com.mapper.TbUserMapper;

@Service("tbUserService")
public class TbUserServiceImpl implements TbUserService{

	@Autowired
	private TbUserMapper tbUserMapper;

	@Override
	public TbUser getUserById(Long id) {
		// 添加查询条件
		TbUserExample exam = new TbUserExample();
		Criteria criteria = exam.createCriteria();
		criteria.andIdEqualTo(id);
		List<TbUser> list = tbUserMapper.selectByExample(exam);
		if (list != null && list.size() > 0) {
			TbUser user = list.get(0);
			return user;
		}
		return null;
	}

	@Override
	public ListData<TbUser> listUsers(int page, int rows) {
		//查询用户列表
		TbUserExample exam = new TbUserExample();
		//分页处理
		PageHelper.startPage(page, rows);
		List<TbUser> list = tbUserMapper.selectByExample(exam);
		ListData dataList = new ListData();
		dataList.setList(list);
		//取记录总条数
		PageInfo<TbUser> pageInfo = new PageInfo<>(list);
		dataList.setTotal(pageInfo.getTotal());
		dataList.setTotalPage(pageInfo.getPages());
		dataList.setPage(page);
		return dataList;
	}

	@Override
	public TbUser deleteUserById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TbUser updateUserById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
